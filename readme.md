# Java Android First Project

Project ini adalah contoh aplikasi android sederhana yang dibuat menggunakan bahasa JAVA dengan architecture pattern MVC (Model-View-Controller).

## Installation

Untuk instalasi, pertama lakukan install android studio versi terbaru untuk menjalankan projek ini. Pada projek ini, ada beberapa library external yang digunakan. Cukup lakukan synchronize pada projek, maka aplikasi android studio akan secara otomatis melakukan instalasi library yang dibutuhkan. Untuk menambahkan library, cukup tambahkan code pada file build.gradle yang terletak pada folder app.

Contoh :

```bash
implementation 'com.squareup.retrofit2:retrofit:2.5.0'
implementation 'com.squareup.retrofit2:converter-gson:2.5.0'
implementation 'com.android.support:recyclerview-v7:28.0.0'
```

Untuk memanggil REST API, di sini library yang digunakan adalah retrofit2. Contoh ada dalam file ApiServiceAuth.java
```
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sumedcoding.com.javaandroidfirstproject.data.model.DefaultResponseModel;

public interface ApiServiceAuth {
    String file = "auth.php?method=";

    @FormUrlEncoded
    @POST(file + "login")
    Call<DefaultResponseModel> login(@Field("username") String username, @Field("password") String password);
}
```

Untuk BaseUrl-nya terletak pada GlobalVariable.
```
public class GlobalVariable extends Application {
    private String baseUrl = "http://10.98.0.154/firstproject/api/controller/";
    private Retrofit retrofit;

    public Retrofit getRetrofit() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(360, TimeUnit.SECONDS)
                .writeTimeout(360, TimeUnit.SECONDS)
                .readTimeout(360, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
```

Contoh cara memanggil REST API ada pada file LoginActivity.java.
```
private void login() {
        Call<DefaultResponseModel> login = apiServiceAuth.login(et_username.getText().toString(), et_password.getText().toString());
        login.enqueue(new Callback<DefaultResponseModel>() {
            @Override
            public void onResponse(Call<DefaultResponseModel> call, Response<DefaultResponseModel> response) {
                if(response.code() == 200){
                    DefaultResponseModel defaultResponseModel = response.body();
                    if(defaultResponseModel.getError() == null){
                        UserModel userModel = gson.fromJson(defaultResponseModel.getResult(), UserModel.class);
                        gv.setUserModel(userModel);
                        redirectHome();
                    }else{
                        showAlert("Login Failed", defaultResponseModel.getError());
                    }
                }else{
                    showAlert("Login Failed", "Oops! Something went wrong.");
                }
            }

            @Override
            public void onFailure(Call<DefaultResponseModel> call, Throwable t) {
                showAlert("Login Failed", "Oops! Something went wrong.");
            }
        });
    }
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[SUMEDCODING.COM](http://sumedcoding.com/)
