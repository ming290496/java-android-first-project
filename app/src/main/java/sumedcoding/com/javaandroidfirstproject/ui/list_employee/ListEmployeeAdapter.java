package sumedcoding.com.javaandroidfirstproject.ui.list_employee;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sumedcoding.com.javaandroidfirstproject.R;
import sumedcoding.com.javaandroidfirstproject.data.model.employee.EmployeeModel;
import sumedcoding.com.javaandroidfirstproject.utilities.CustomClickListener;
import sumedcoding.com.javaandroidfirstproject.utilities.GlobalVariable;

public class ListEmployeeAdapter extends RecyclerView.Adapter<ListEmployeeAdapter.ViewHolder> {
    private Context ctxt;
    private CustomClickListener listener;
    private GlobalVariable gv;
    private ArrayList<EmployeeModel> employeeModels;

    public ListEmployeeAdapter(Context ctxt, ArrayList<EmployeeModel> employeeModels, CustomClickListener listener) {
        this.ctxt = ctxt;
        this.listener = listener;
        this.employeeModels = employeeModels;
        gv = (GlobalVariable) ctxt.getApplicationContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_employee, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, viewHolder.getPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setIsRecyclable(false);

        viewHolder.tv_id.setText(String.valueOf(employeeModels.get(i).getId()));
        viewHolder.tv_fullname.setText(employeeModels.get(i).getFullname());
    }

    @Override
    public int getItemCount() {
        return employeeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_id;
        private TextView tv_fullname;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_fullname = itemView.findViewById(R.id.tv_fullname);
        }
    }
}
