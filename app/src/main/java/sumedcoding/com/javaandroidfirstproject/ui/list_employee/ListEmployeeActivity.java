package sumedcoding.com.javaandroidfirstproject.ui.list_employee;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import sumedcoding.com.javaandroidfirstproject.R;
import sumedcoding.com.javaandroidfirstproject.data.apiService.ApiServiceEmployee;
import sumedcoding.com.javaandroidfirstproject.data.model.DefaultResponseModel;
import sumedcoding.com.javaandroidfirstproject.data.model.employee.EmployeeModel;
import sumedcoding.com.javaandroidfirstproject.ui.home.HomeActivity;
import sumedcoding.com.javaandroidfirstproject.utilities.CustomClickListener;
import sumedcoding.com.javaandroidfirstproject.utilities.GlobalVariable;

public class ListEmployeeActivity extends AppCompatActivity {
    private Context ctxt;
    private GlobalVariable gv;
    private Retrofit retrofit;
    private Gson gson;

    private ApiServiceEmployee apiServiceEmployee;

    private RecyclerView re_employee;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_employee);

        ctxt = ListEmployeeActivity.this;
        gv = (GlobalVariable) getApplicationContext();
        retrofit = gv.getRetrofit();
        gson = new Gson();

        apiServiceEmployee = retrofit.create(ApiServiceEmployee.class);

        initUI();
        initValue();
        initEvent();
    }

    private void initEvent() {

    }

    private void initValue() {
        fetchEmployee();
    }

    private void initUI() {
        re_employee = findViewById(R.id.re_employee);
    }

    private void fetchEmployee() {
        Call<DefaultResponseModel> call = apiServiceEmployee.SelectMsEmployee();
        call.enqueue(new Callback<DefaultResponseModel>() {
            @Override
            public void onResponse(Call<DefaultResponseModel> call, Response<DefaultResponseModel> response) {
                if (response.code() == 200) {
                    DefaultResponseModel defaultResponseModel = response.body();
                    if (defaultResponseModel.getError() == null) {
                        ArrayList<EmployeeModel> employeeModels = new ArrayList<>();

                        employeeModels = gson.fromJson(defaultResponseModel.getResult(), new TypeToken<List<EmployeeModel>>() {
                        }.getType());
                        populateEmployee(employeeModels);
                    } else {
                        showAlert("Login Failed", defaultResponseModel.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<DefaultResponseModel> call, Throwable t) {
                showAlert("Login Failed", "Oops! Something went wrong.");
            }
        });
    }

    private void populateEmployee(final ArrayList<EmployeeModel> employeeModels) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(ctxt);
        ListEmployeeAdapter adapter = new ListEmployeeAdapter(ctxt, employeeModels, new CustomClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                popupDetailEmployee(employeeModels.get(position));
            }
        });
        re_employee.setLayoutManager(layoutManager);
        re_employee.setAdapter(adapter);
    }

    private void popupDetailEmployee(EmployeeModel employeeModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.popup_list_employee_detail, null);

        TextView tv_title_fullname = view.findViewById(R.id.tv_title_fullname);
        TextView tv_id = view.findViewById(R.id.tv_id);
        TextView tv_fullname = view.findViewById(R.id.tv_fullname);
        TextView tv_gender = view.findViewById(R.id.tv_gender);
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_language = view.findViewById(R.id.tv_language);
        TextView tv_job_title = view.findViewById(R.id.tv_job_title);
        TextView tv_university = view.findViewById(R.id.tv_university);

        tv_title_fullname.setText(employeeModel.getTitle() + " " + employeeModel.getFullname());
        tv_id.setText(String.valueOf(employeeModel.getId()));
        tv_fullname.setText(employeeModel.getFullname());
        tv_gender.setText(employeeModel.getGender());
        tv_title.setText(employeeModel.getTitle());
        tv_language.setText(employeeModel.getLanguage());
        tv_job_title.setText(employeeModel.getJob_title());
        tv_university.setText(employeeModel.getUniversity());

        builder.setView(view);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ctxt, HomeActivity.class));
        finish();
    }
}