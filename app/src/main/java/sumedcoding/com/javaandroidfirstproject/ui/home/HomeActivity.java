package sumedcoding.com.javaandroidfirstproject.ui.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import sumedcoding.com.javaandroidfirstproject.R;
import sumedcoding.com.javaandroidfirstproject.data.model.auth.UserModel;
import sumedcoding.com.javaandroidfirstproject.ui.auth.LoginActivity;
import sumedcoding.com.javaandroidfirstproject.ui.list_employee.ListEmployeeActivity;
import sumedcoding.com.javaandroidfirstproject.utilities.GlobalVariable;

public class HomeActivity extends AppCompatActivity {
    private Context ctxt;
    private GlobalVariable gv;
    private Retrofit retrofit;
    private Gson gson;

    private TextView tv_welcome;
    private Button btn_view_employee;
    private Button btn_logout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ctxt = HomeActivity.this;
        gv = (GlobalVariable) getApplicationContext();
        retrofit = gv.getRetrofit();
        gson = new Gson();

        initUI();
        initValue();
        initEvent();
    }

    private void initEvent() {
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        btn_view_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ctxt, ListEmployeeActivity.class));
                finish();
            }
        });
    }

    private void initValue() {
        UserModel userModel = gv.getUserModel();
        tv_welcome.setText("Welcome, " + userModel.getFullname());
    }

    private void initUI() {
        tv_welcome = findViewById(R.id.tv_welcome);
        btn_view_employee = findViewById(R.id.btn_view_employee);
        btn_logout = findViewById(R.id.btn_logout);
    }

    @Override
    public void onBackPressed() {
        logout();
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(ctxt, LoginActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("No", (DialogInterface.OnClickListener)null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
