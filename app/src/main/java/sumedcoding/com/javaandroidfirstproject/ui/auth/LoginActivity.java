package sumedcoding.com.javaandroidfirstproject.ui.auth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import sumedcoding.com.javaandroidfirstproject.R;
import sumedcoding.com.javaandroidfirstproject.data.apiService.ApiServiceAuth;
import sumedcoding.com.javaandroidfirstproject.data.model.DefaultResponseModel;
import sumedcoding.com.javaandroidfirstproject.data.model.auth.UserModel;
import sumedcoding.com.javaandroidfirstproject.ui.home.HomeActivity;
import sumedcoding.com.javaandroidfirstproject.utilities.GlobalVariable;

public class LoginActivity extends AppCompatActivity {
    private Context ctxt;
    private GlobalVariable gv;
    private Retrofit retrofit;
    private Gson gson;

    private ApiServiceAuth apiServiceAuth;

    private Button btn_login;
    private EditText et_username;
    private EditText et_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ctxt = LoginActivity.this;
        gv = (GlobalVariable) getApplicationContext();
        retrofit = gv.getRetrofit();
        gson = new Gson();

        apiServiceAuth = retrofit.create(ApiServiceAuth.class);

        initUI();
        initValue();
        initEvent();
    }

    private void initEvent() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void initValue() {
        setFieldDev();
    }

    private void initUI() {
        btn_login = findViewById(R.id.btn_login);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
    }

    private void login() {
        Call<DefaultResponseModel> login = apiServiceAuth.login(et_username.getText().toString(), et_password.getText().toString());
        login.enqueue(new Callback<DefaultResponseModel>() {
            @Override
            public void onResponse(Call<DefaultResponseModel> call, Response<DefaultResponseModel> response) {
                if(response.code() == 200){
                    DefaultResponseModel defaultResponseModel = response.body();
                    if(defaultResponseModel.getError() == null){
                        UserModel userModel = gson.fromJson(defaultResponseModel.getResult(), UserModel.class);
                        gv.setUserModel(userModel);
                        redirectHome();
                    }else{
                        showAlert("Login Failed", defaultResponseModel.getError());
                    }
                }else{
                    showAlert("Login Failed", "Oops! Something went wrong.");
                }
            }

            @Override
            public void onFailure(Call<DefaultResponseModel> call, Throwable t) {
                showAlert("Login Failed", "Oops! Something went wrong.");
            }
        });
    }

    private void showAlert(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void redirectHome(){
        Intent intent = new Intent(ctxt, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void setFieldDev(){
        et_username.setText("admin");
        et_password.setText("admin");
    }
}
