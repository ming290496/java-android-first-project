package sumedcoding.com.javaandroidfirstproject.data.apiService;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sumedcoding.com.javaandroidfirstproject.data.model.DefaultResponseModel;

public interface ApiServiceEmployee {
    String file = "employee.php?method=";

    @POST(file + "select_ms_employee")
    Call<DefaultResponseModel> SelectMsEmployee();
}
