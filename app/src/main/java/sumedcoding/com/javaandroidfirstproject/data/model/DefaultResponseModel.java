package sumedcoding.com.javaandroidfirstproject.data.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

public class DefaultResponseModel {
    @SerializedName("result")
    private JsonElement result;
    @SerializedName("error")
    private String error;

    public JsonElement getResult() {
        return result;
    }

    public String getError() {
        return error;
    }
}
