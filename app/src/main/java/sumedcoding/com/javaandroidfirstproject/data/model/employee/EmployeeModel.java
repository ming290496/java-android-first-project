package sumedcoding.com.javaandroidfirstproject.data.model.employee;

import com.google.gson.annotations.SerializedName;

public class EmployeeModel {
    @SerializedName("id")
    private long id;
    @SerializedName("fullname")
    private String fullname;
    @SerializedName("gender")
    private String gender;
    @SerializedName("title")
    private String title;
    @SerializedName("language")
    private String language;
    @SerializedName("job_title")
    private String job_title;
    @SerializedName("university")
    private String university;

    public EmployeeModel(long id, String fullname, String gender, String title, String language, String job_title, String university) {
        this.id = id;
        this.fullname = fullname;
        this.gender = gender;
        this.title = title;
        this.language = language;
        this.job_title = job_title;
        this.university = university;
    }

    public long getId() {
        return id;
    }

    public String getFullname() {
        return fullname;
    }

    public String getGender() {
        return gender;
    }

    public String getTitle() {
        return title;
    }

    public String getLanguage() {
        return language;
    }

    public String getJob_title() {
        return job_title;
    }

    public String getUniversity() {
        return university;
    }
}
