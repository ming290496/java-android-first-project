package sumedcoding.com.javaandroidfirstproject.data.model.auth;

import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("id")
    private long id;
    @SerializedName("username")
    private String username;
    @SerializedName("fullname")
    private String fullname;
    @SerializedName("address")
    private String address;
    @SerializedName("email")
    private String email;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFullname() {
        return fullname;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }
}
