package sumedcoding.com.javaandroidfirstproject.data.apiService;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sumedcoding.com.javaandroidfirstproject.data.model.DefaultResponseModel;

public interface ApiServiceAuth {
    String file = "auth.php?method=";

    @FormUrlEncoded
    @POST(file + "login")
    Call<DefaultResponseModel> login(@Field("username") String username, @Field("password") String password);
}
